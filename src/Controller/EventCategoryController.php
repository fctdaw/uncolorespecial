<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\EventCategory;
use App\Entity\Event;

class EventCategoryController extends AbstractController
{

    public function category(EventCategory $category = NULL)
    {
      if ($category == NULL || !$category){
        return $this->RedirectToRoute('not-found',[
          "message" => "La página de esta categoría no existe."
        ]);
      }

      $event_repo = $this->getDoctrine()->getRepository(Event::class);
			$events = $event_repo->findBy(array('category' => $category));

      return $this->render('event_category/index.html.twig', [
          'events' => $events,
          'category' => $category,
      ]);
    }

    public function getCategories(){

      $categories_repo = $this->getDoctrine()->getRepository(EventCategory::class);
      $categories = $categories_repo->findAll();



        return $this->render('event/index.html.twig', [
            'categories' => $categories,
        ]);

    }
}
