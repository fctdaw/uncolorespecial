<?php

namespace App\Controller;

// Se importa por defecto para extender del controlador "base" facilitado por symfony
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Importamos las entidades que vamos a utilizar
use App\Entity\User;
use App\Entity\Rol;
use App\Entity\Event;
use App\Entity\EventCategory;
use App\Entity\Business;
use App\Entity\BusinessCategory;
use App\Entity\Review;
use App\Controller\MailerController;
use App\Controller\HelperController;
use Symfony\Component\Mailer\MailerInterface;

// Importamos las clases relativas a respuestas y peticiones HTTP
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// Importamos las clases para los tipos utilizados en el formulario
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

// Importamos la clase Constraints para realizar validaciones sobre el formulario
use Symfony\Component\Validator\Constraints as Assert;

// Importamos la clase para codificar la contraseña y hacer el login
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Security;


class UserController extends AbstractController{

	public function registerUser(UserPasswordEncoderInterface $encoder, Request $request){

		$user_repo = $this->getDoctrine()->getRepository(User::class);

		// Creamos el formulario

      $form = $this->createFormBuilder()
						->add('email', EmailType::class,['label' => 'Email:'])
						->add('password', PasswordType::class,['label' => 'Contraseña:'])
						->add('confirmPass', PasswordType::class,['label' => 'Repite la contraseña:'])
						->add('name', TextType::class,['label' => 'Nombre:'])
						->add('surnames', TextType::class,['label' => 'Apellidos:'])
						->add('alias', TextType::class,['label' => 'Nombre de usuario:'])
          				->getForm();

		// Comprobamos la solicitud
		$form->handleRequest($request);
      // Comrpobamos si el formulario se ha registrado y es valido
      if ($form->isSubmitted() && $form->isValid()) {

			$pass = $form->get('password')->getData();
			$cPass = $form->get('confirmPass')->getData();

			if ($user_repo->findBy(['email' => $form->get('email')->getData()])){
				return $this->render('user/register.html.twig', [
           			'title' => 'Registro de usuarios',
					'pageTitle' => 'Error de registro',
           			'form' => $form->createView(),
					'email_exist' => 'Ya existe un usuario con este email.']);
			}

			if ($pass != $cPass){
				return $this->render('user/register.html.twig', [
           			'title' => 'Registro de usuarios',
					'pageTitle' => 'Error de registro',
           			'form' => $form->createView(),
					'error' => 'Las contraseñas no son iguales.']);
			}

			$rol_repo = $this->getDoctrine()->getRepository(Rol::class);
			$rol = $rol_repo->findOneBy(array('name' => 'ROLE_USER'));

			$user = new User();
			$user
				->setEmail($form->get('email')->getData())
				->setName($form->get('name')->getData())
				->setSurnames($form->get('surnames')->getData())
				->setAlias($form->get('alias')->getData())
				->setPassword($form->get('password')->getData())
				->setActive(1)
				->setImage('user-default.jpg')
				->setRol($rol);

			$encodedPw = $encoder->encodePassword($user, $user->getPassword());
			$user->setPassword($encodedPw);

			$entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

			return $this->RedirectToRoute('welcome-message', ['email' => $form->get('email')->getData()]);


        } else {

            // Si aún no se ha enviado el formulario devolvemos la vista
            return $this->render('user/register.html.twig', [
                'title' => 'Registro de usuarios',
                'form' => $form->createView(),
            ]);

        }

	} // FIN registerUser


	public function editUser(UserPasswordEncoderInterface $encoder, Request $request, Security $security, HelperController $helper){

    	if ($security->getUser()->getActive() == 0) {
    		return $this->RedirectToRoute('logout');
    	}

		$user_repo = $this->getDoctrine()->getRepository(User::class);

		$reviews_repo = $this->getDoctrine()->getRepository(Review::class);
		$review = $reviews_repo->findOneBy(['active' => 1, 'user' => $security->getUser()], ['id' => 'DESC']);

		$event_repo = $this->getDoctrine()->getRepository(Event::class);
		$events = $event_repo->findBy(['active' => 1], ['id' => 'DESC'], $limit = 3 );

		// Creamos el formulario
  		$form = $this->createFormBuilder()
			->add('alias', TextType::class,['label' => 'Nombre de usuario:'])
			->add('name', TextType::class,['label' => 'Nombre:'])
			->add('surnames', TextType::class,['label' => 'Apellidos:'])
			->add('birthDate', BirthdayType::class,['label' => 'Fecha de nacimiento:'])
			->add('direccion', TextType::class,['label' => 'Dirección:','required' => false])
			->add('phone', TextType::class,['label' => 'Teléfono:','required' => false])
			->add('oldPassword', PasswordType::class,['label' => 'Contraseña:','required' => false])
			->add('newPassword', PasswordType::class,['label' => 'Repite la contraseña:','required' => false])
        ->getForm();

		// Comprobamos la solicitud
		$form->handleRequest($request);

		// Comrpobamos si el formuario se ha registrado y es valido
		if ($form->isSubmitted() && $form->isValid()) {

			$user = $security->getUser();

			$oldPass = $form->get('oldPassword')->getData();
			$newPass = $form->get('newPassword')->getData();

			$user
				->setName($form->get('name')->getData())
				->setSurnames($form->get('surnames')->getData())
				->setAlias($form->get('alias')->getData())
				->setDireccion($form->get('direccion')->getData())
				->setBirthDate($form->get('birthDate')->getData())
				->setPhone($form->get('phone')->getData());

			if ($newPass){
				if (!$encoder->isPasswordValid($user,$oldPass)){
					return $this->render('user/edit.html.twig', [
									'title' => 'Editar tus datos',
						'pageTitle' => 'Error en el formulario',
									'form' => $form->createView(),
						'error' => 'La contraseña antigua es incorrecta.']);
				}
				$encodedPw = $encoder->encodePassword($user, $newPass);
				$user->setPassword($encodedPw);
			}



			$entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

			return $this->render('user/edit.html.twig', [
				'title' => 'Editar tus datos',
				'pageTitle' => 'Éxito',
				'form' => $form->createView(),
				'success' => 'El usuario se ha modificado.',
				'review' => $review,
				'events' => $events
			]);

		} else {

			return $this->render('user/edit.html.twig',[
				'form' => $form->createView(),
				'review' => $review,
				'events' => $events
			]);
		}



	} // FIN editUser



    public function login(AuthenticationUtils $authenticationUtils, Security $security){

        $user = $security->getUser();

        if ($user) {
           return $this->RedirectToRoute('edit-user');
        }

    	$error = $authenticationUtils->getLastAuthenticationError();
    	$lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('user/index.html.twig', [
            'error' => $error,
            'lastUsername' => $lastUsername
        ]);
    } // FIN login


    public function updateImage(Security $security, Request $request){
        $user = $security->getUser();
        $form = $this->createFormBuilder()
							->add('image', FileType::class,['label' => 'Foto de perfil:'])
            ->getForm();

                    // Comprobamos la solicitud
        $form->handleRequest($request);

        // So el formuario se ha registrado y es valido procesamos la información para subir la image
        if ($form->isSubmitted() && $form->isValid()) {

            // Recuperamos el archivo
            $image = $form->get('image')->getData();

            // Revisamos la extensión y creamos el nombre del archivo
            $imageName = $user->getId() . '.' . $image->guessExtension();

            // Movemos el archivo donde queremos que esté
            $image->move('assets/img/user', $imageName);

            // Actualizamos el registro en la bbdd con el nombre de la imagen correcto
            $em = $this->getDoctrine()->getManager();
            $user->setImage($imageName);
            $em->persist($user);
            $em->flush();
            return $this->RedirectToRoute('edit-user');

        } else {

            // Si aún no se ha enviado el formulario devolvemos la vista

            return $this->render('user/upload-foto.html.twig', [
            	'form' => $form->createView()

        	]);
        }

    } // FIN login



        public function getUsers($page,Request $request){

            $user_repo = $this->getDoctrine()->getRepository(User::class);


            $users = $user_repo->findBy(
                                    array(),
                                    array('id' => 'DESC'),
                                    5,
                                    5 * ($page - 1)
                                );
						$totalPages = ceil(count($user_repo->findAll())/5);

			if ($request->query->get('modalTitle')) {
				return $this->render('admin/users.html.twig', [
	                'users' => $users,
					'totalPages' => $totalPages,
					'currentPage' => $page,
					'modalTitle' => $request->query->get('modalTitle'),
					'modalMessage' => $request->query->get('modalMessage'),
					'icon' => $request->query->get('icon'),
					'targeturl' => $request->query->get('targeturl'),
	            ]);
			}

            return $this->render('admin/users.html.twig', [
                'users' => $users,
				'totalPages' => $totalPages,
				'currentPage' => $page
            ]);
        }





	public function activeUser(User $user){

		if ($user->getActive())
			$user->setActive(0);
		else
			$user->setActive(1);

		$em = $this->getDoctrine()->getManager();
		$em->persist($user);
		$em->flush();

		return $this->RedirectToRoute('admin-users');

	}

	public function newUser(Request $request,UserPasswordEncoderInterface $encoder, HelperController $helper){

        $rol_repo = $this->getDoctrine()->getRepository(Rol::class);
        $rols = $rol_repo->findAll();

        $form = $this->createFormBuilder()
			->add('alias', TextType::class,['label' => 'Nombre de usuario:'])
			->add('email', EmailType::class,['label' => 'Nombre de usuario:'])
			->add('image', FileType::class,['label' => 'Imagen:','required' => false])
			->add('name', TextType::class,['label' => 'Nombre:'])
			->add('surnames', TextType::class,['label' => 'Apellidos:','required' => false])
			->add('birthDate', BirthdayType::class,[
					'label' => 'Fecha de nacimiento:',
					'required' => false,
					'placeholder' => [
							'year' => 'Año',
							'month' => 'Mes',
							'day' => 'Día',
						]])
			->add('direccion', TextType::class,['label' => 'Dirección:','required' => false,])
			->add('phone', TextType::class,['label' => 'Teléfono:','required' => false])
			->add('password', PasswordType::class,['label' => 'Contraseña:'])
			->add('active', CheckboxType::class, ['required' => false])
			->add('confirmPass', PasswordType::class,['label' => 'Repite la contraseña:'])
			->add('rol', ChoiceType::class,[
          'label' => 'Rol:',
          'placeholder' => 'Elige un Rol',
          'choices' => $rols,
          'choice_value' => 'id',
          'choice_label' => 'shown_name'])
				->getForm();

        // Comprobamos la solicitud
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

			$user_repo = $this->getDoctrine()->getRepository(User::class);

			$pass = $form->get('password')->getData();
			$cPass = $form->get('confirmPass')->getData();

			if ($user_repo->findBy(['email' => $form->get('email')->getData()])){
				return $this->render('admin/create-user.html.twig', [
								'form' => $form->createView(),
					'message' => 'Ya existe un usuario con este email.',
					'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>'
				]);
			}

			$slug = $form->get('name')->getData();
	        $slug = $helper->curateSlug($slug);

			// Recuperamos el archivo
			$image = $form->get('image')->getData();

			if ($image){
				// Revisamos la extensión y creamos el nombre del archivo
				$image_name = $slug . '.' . $image->guessExtension();

				// Movemos el archivo donde queremos que esté
				$image->move('assets/img/user', $image_name);
			}


			if ($pass != $cPass){
				return $this->render('admin/create-user.html.twig', [
								'form' => $form->createView(),
					'message' => 'Las contraseñas no son iguales.',
					'message_icon' => '<i class="fas fa-exclamation-triangle text-warning"></i>'
				]);
			}

			$user = new User();
			$user
				->setName($form->get('name')->getData())
				->setSurnames($form->get('surnames')->getData())
				->setEmail($form->get('email')->getData())
				->setAlias($form->get('alias')->getData())
				->setDireccion($form->get('direccion')->getData())
				->setBirthDate($form->get('birthDate')->getData())
				->setPhone($form->get('phone')->getData())
	            ->setRol($form->get('rol')->getData())
	            ->setActive($form->get('active')->getData())
				->setPassword($pass);

			if ($image){
				 $user->setImage($image_name);
			}

			$entityManager= $this->getDoctrine()->getManager();
			$entityManager->persist($user);
			$entityManager->flush();

			// Si se ha creado el usuario se redirige al listado de usuarios
			return $this->RedirectToRoute('admin-users',[
				'targeturl' => 'usuarios', // '--' sustituye a '/' para no tener problemas de url
				'modalTitle' => 'Creado con éxito',
				'modalMessage' => 'Usuario creado con éxito',
				'icon' => 'success' //puede ser info,success,danger
			]);

        } else {

			// Si aún no se ha enviado el formulario devolvemos la vista
			return $this->render('admin/create-user.html.twig', [
			'form' => $form->createView()
			]);
	    }
	}



	public function adminEditUser(UserPasswordEncoderInterface $encoder, Request $request, User $user, HelperController $helper){

		$rol_repo = $this->getDoctrine()->getRepository(Rol::class);
		$rols = $rol_repo->findBy([],['id' => 'DESC']);

		$form = $this->createFormBuilder()
				->add('alias', TextType::class,['label' => 'Nombre de usuario:'])
				->add('email', EmailType::class,['label' => 'Nombre de usuario:'])
				->add('image', FileType::class,['label' => 'Imagen:','required' => false])
				->add('name', TextType::class,['label' => 'Nombre:'])
				->add('surnames', TextType::class,['label' => 'Apellidos:','required' => false])
				->add('birthDate', BirthdayType::class,[
						'label' => 'Fecha de nacimiento:',
						'required' => false,
						'placeholder' => [
								'year' => 'Año',
								'month' => 'Mes',
								'day' => 'Día',
							]])
				->add('direccion', TextType::class,['label' => 'Dirección:','required' => false,])
				->add('phone', TextType::class,['label' => 'Teléfono:','required' => false])
				->add('password', PasswordType::class,['label' => 'Contraseña:'])
				->add('active', CheckboxType::class, ['required' => false])
				->add('confirmPass', PasswordType::class,['label' => 'Repite la contraseña:'])
				->add('rol', EntityType::class,[
						'class' => Rol::class,
						'choice_label' => 'shown_name',
						'data' => $user->getRol()
					])
					->getForm();

		// Comprobamos la solicitud
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$user_repo = $this->getDoctrine()->getRepository(User::class);

			$pass = $form->get('password')->getData();
			$cPass = $form->get('confirmPass')->getData();

			$emailNewAndExists = false;
			if ($user_repo->findBy(['email' => $form->get('email')->getData()])){
				if ($user->getEmail() !=  $form->get('email')->getData()){
					$emailNewAndExists = true;

				}
			}

			if ($emailNewAndExists){
				return $this->render('admin/edit-user.html.twig', [
								'form' => $form->createView(),
					'message' => 'Otro usuario ya tiene este correo.',
					'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>',
					'user' => $user
				]);
			}

			$aliasNewAndExists = false;
			if ($user_repo->findBy(['alias' => $form->get('alias')->getData()])){
				if ($user->getAlias() !=  $form->get('alias')->getData()){
					$aliasNewAndExists = true;
				}
			}

			if ($aliasNewAndExists){
				return $this->render('admin/edit-user.html.twig', [
								'form' => $form->createView(),
					'message' => 'Otro usuario ya tiene este alias.',
					'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>',
					'user' => $user
				]);
			}

			$slug = $form->get('name')->getData();
	        $slug = $helper->curateSlug($slug);

			// Recuperamos el archivo
			$image = $form->get('image')->getData();
			$password = $form->get('password')->getData();

			if ($image){
				// Revisamos la extensión y creamos el nombre del archivo
				$image_name = $slug . '.' . $image->guessExtension();

				// Movemos el archivo donde queremos que esté
				$image->move('assets/img/user', $image_name);
			}


			if ($pass != $cPass){
				return $this->render('admin/edit-user.html.twig', [
								'form' => $form->createView(),
					'message' => 'Las contraseñas no son iguales.',
					'message_icon' => '<i class="fas fa-exclamation-triangle text-warning"></i>',
					'user' => $user
				]);
			}

			$user
				->setName($form->get('name')->getData())
				->setSurnames($form->get('surnames')->getData())
				->setEmail($form->get('email')->getData())
				->setAlias($form->get('alias')->getData())
				->setDireccion($form->get('direccion')->getData())
				->setBirthDate($form->get('birthDate')->getData())
				->setPhone($form->get('phone')->getData())
				->setRol($form->get('rol')->getData())
				->setActive($form->get('active')->getData());

			if ($password){

				$encodedPw = $encoder->encodePassword($user, $password);
				$user->setPassword($encodedPw);
			}


			if ($image){
				 $user->setImage($image_name);
			}


			$entityManager= $this->getDoctrine()->getManager();
			$entityManager->persist($user);
			$entityManager->flush();


			// Si aún no se ha enviado el formulario devolvemos la vista
			return $this->render('admin/edit-user.html.twig', [
				'form' => $form->createView(),
				'message' => 'Usuario editado con éxito!',
				'message_icon' => '<i class="fas fa-check text-success"></i>',
				'user' => $user
			]);

		} else {
			return $this->render('admin/edit-user.html.twig',[
				'form' => $form->createView(),
				'user' => $user
			]);
		}
  	} // FIN adminEditUser




}
