<?php

namespace App\Controller;
// Importamos las clases relativas a respuestas y peticiones HTTP
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


//Importamos los campos de formularios que vamos a usar
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Controller\HelperController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Category;
use App\Entity\ContentBlock;
use App\Entity\BlockType;
use App\Entity\Page;
use Symfony\Component\Security\Core\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController {

	public function getCategories(){

		$category_repo = $this->getDoctrine()->getRepository(Category::class);
		$categories = $category_repo->findAll();

		$page_repo = $this->getDoctrine()->getRepository(Page::class);
		$posts = $page_repo->findBy([],['id' => 'DESC'], 2);
        return $this->render('blog/index.html.twig', [
            'categories' => $categories,
            'posts' => $posts
        ]);
	}

	public function newBlog(Request $request, Security $security, HelperController $helper){

	    $category_repo = $this->getDoctrine()->getRepository(Category::class);
	    $categories = $category_repo->findAll();

	    $form = $this->createFormBuilder()
	      ->add('title', TextType::class)
	      ->add('slug', TextType::class)
	      ->add('meta_title', TextType::class)
	      ->add('meta_description', TextareaType::class)
	      ->add('image', FileType::class)
	      ->add('category', ChoiceType::class,[
	              'placeholder' => 'Elige una categoría',
	              'choices' => $categories,
	              'choice_value' => 'id',
	              'choice_label' => 'title'])
	      ->add('active', CheckboxType::class, ['required' => false])

	        ->getForm();

	    // Comprobamos la solicitud
	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {

	      $pages_repo = $this->getDoctrine()->getRepository(Page::class);

	      if ($pages_repo->findBy(['slug' => $form->get('slug')->getData()])){
	        return $this->render('admin/create-blog.html.twig', [
	                'form' => $form->createView(),
	          'message' => 'Ya existe una página con este slug.',
	          'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>'
	        ]);
	      }

	      $slug = $form->get('slug')->getData();
		  $slug = $helper->curateSlug($slug);

	      // Recuperamos el archivo
	      $image = $form->get('image')->getData();

	      // Revisamos la extensión y creamos el nombre del archivo
	      $image_name = $slug . '.' . $image->guessExtension();

	      // Movemos el archivo donde queremos que esté
	      $image->move('assets/img/blog/feature', $image_name);

	      $page = new Page();
	      $page
	        ->setTitle($form->get('title')->getData())
	        ->setMetaDescription($form->get('meta_description')->getData())
	        ->setMetaTitle($form->get('meta_title')->getData())
	        ->setImage($image_name)
	        ->setSlug($slug)
	        ->setActive($form->get('active')->getData())
	        ->setCategory($form->get('category')->getData())
	        ->setCategory($form->get('category')->getData())
	        ->setUser($security->getUser());



	      $entityManager= $this->getDoctrine()->getManager();
	      $entityManager->persist($page);
	      $entityManager->flush();

		  // Si se ha creado el usuario se redirige al listado de usuarios
		  return $this->RedirectToRoute('admin-edit-blog',[
			  'id' => $page->getId(),
			  'targeturl' => $page->getId(), // '--' sustituye a '/' para no tener problemas de url
			  'modalTitle' => 'Creado con éxito',
			  'modalMessage' => 'Blog creado, puedes añadir páginas ahora',
			  'icon' => 'success' //puede ser info,success,danger
		  ]);

	    } else {

	      // Si aún no se ha enviado el formulario devolvemos la vista
	      return $this->render('admin/create-blog.html.twig', [
	        'form' => $form->createView(),
	      ]);

	    }
	}

	public function activeBlog(Page $page){

		if ($page->getActive())
			$page->setActive(0);
		else
			$page->setActive(1);

		$em = $this->getDoctrine()->getManager();
		$em->persist($page);
		$em->flush();

	  return $this->RedirectToRoute('admin-blogs');

	}

	public function activeContentBlock(ContentBlock $contentBlock){

		if ($contentBlock->getActive())
			$contentBlock->setActive(0);
		else
			$contentBlock->setActive(1);

		$em = $this->getDoctrine()->getManager();
		$em->persist($contentBlock);
		$em->flush();

	  return $this->RedirectToRoute('admin-edit-blog', ['id' => $contentBlock->getPage()->getId()]);

	}

    public function getPosts($page){
        $page_repo = $this->getDoctrine()->getRepository(Page::class);


        $blogs = $page_repo->findBy(
                                array(),
                                array('id' => 'ASC'),
                                5,
                                5 * ($page - 1)
                            );
					$totalPages = ceil(count($page_repo->findAll())/5);

        return $this->render('admin/blogs.html.twig', [
            'blogs' => $blogs,
			'totalPages' => $totalPages,
			'currentPage' => $page
        ]);
    }

	public function adminEditBlog(Request $request, Page $blog){

		$blog_repo = $this->getDoctrine()->getRepository(Page::class);
		$category_repo = $this->getDoctrine()->getRepository(Category::class);

	    $blog_categories = $category_repo->findAll();

	    $form = $this->createFormBuilder()
			->add('title', TextType::class)
		   	->add('category', EntityType::class,[
				'class' => Category::class,
				'choice_label' => 'title',
				'data' => $blog->getCategory()
			   	])
			->add('meta_title', TextType::class,['required' => false])
			->add('meta_description', TextareaType::class,	['required' => false])
			->add('active', CheckboxType::class, ['required' => false])
			->add('image', FileType::class)
	    ->getForm();




	    // Comprobamos la solicitud
	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {

	        $titleNewAndExists = false;
	        if ($blog_repo->findBy(['title' => $form->get('title')->getData()])){
	            if ($blog->getTitle() !=  $form->get('title')->getData()){
	                $titleNewAndExists = true;
	            }
	        }

	        if ($titleNewAndExists){
	            return $this->render('admin/edit-blog.html.twig', [
	                'form' => $form->createView(),
	                'message' => 'Otra página de blog ya tiene este título.',
	                'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>',
					'blog' => $blog
	            ]);
	        }


	        $blog
			  ->setTitle($form->get('title')->getData())
			  ->setMetaDescription($form->get('meta_description')->getData())
			  ->setMetaTitle($form->get('meta_title')->getData())
			  ->setActive($form->get('active')->getData())
			  ->setCategory($form->get('category')->getData())
			  ->setCategory($form->get('category')->getData());



	        // Recuperamos el archivo
	        $image = $form->get('image')->getData();

	        if ($image){
	            // Revisamos la extensión y creamos el nombre del archivo
	            $image_name = $blog->getImage();

	            // Movemos el archivo donde queremos que esté
	            $image->move('assets/img/blog/feature', $image_name);
	        }

			$entityManager= $this->getDoctrine()->getManager();
	        $entityManager->persist($blog);
	        $entityManager->flush();

	        return $this->render('admin/edit-blog.html.twig', [
	            'form' => $form->createView(),
	            'message' => 'Blog editado con éxito!',
	            'message_icon' => '<i class="fas fa-check text-success"></i>',
	            'blog' => $blog,
	            'contentBlocks' => $this->getContentBlocks($blog)
	        ]);

	    } else {

	      // Si aún no se ha enviado el formulario devolvemos la vista
	      return $this->render('admin/edit-blog.html.twig', [
	        'form' => $form->createView(),
	        'blog' => $blog,
            'contentBlocks' => $this->getContentBlocks($blog)
	      ]);

	    }

	}

	// Devuelve los bloques de contenido de una página
    public function getContentBlocks($page){
        $blocks_repo = $this->getDoctrine()->getRepository(ContentBlock::class);
        return $blocks_repo->findBy(['page' => $page]);
    }

    public function getPost(Page $page){

		if ($page == NULL || !$page) {
			return $this->RedirectToRoute('not-found',[
				"message" => "El post no existe."
			]);
		} else if ($page->getActive() == false) {
			return $this->RedirectToRoute('not-found',[
				"message" => "Este post no está activo actualmente."
			]);
		}

    	return $this->render('blog/post.html.twig', [
	        'post' => $page,
            'contentBlocks' => $this->getContentBlocks($page)
	      ]);
    }

    public function adminEditContentBlock(ContentBlock $block, Request $request){

        $blocks_type = $this->getDoctrine()->getRepository(BlockType::class);

       	$form = $this->createFormBuilder()
			->add('image', FileType::class,[
				'label' => 'Foto:',
				'required' => false ])
			->add('content', TextareaType::class,[
				'label' => 'Contenido:',
				'required' => false])
			->add('blockType', EntityType::class,[
				  'class' => BlockType::class,
				  'required' => false,
				  'choice_label' => 'name',
				  'data' => $block->getBlockType()
			  	])
            ->getForm();

                    // Comprobamos la solicitud
        $form->handleRequest($request);



        if ( $form->isSubmitted() && $form->isValid() ) {

        	$content_block = new ContentBlock();

        	$image = $form->get('image')->getData();

	        if ($image){
				$image_name = '1' . '.jpg';
				$counter = 1;

				while (file_exists ('assets/img/blog/content_blocks/' . $block->getPage()->getId() . '/' . $image_name  ) ) {
					$counter = $counter + 1;
					$image_name = $counter . '.jpg';
				}
	            // Movemos el archivo donde queremos que esté
	            $image->move('assets/img/blog/content_blocks/' . $block->getPage()->getId() . '/', $image_name);
	        }

	        $blockType = $blocks_type->findOneBy(['id' => $form->get('blockType')->getData()]);
	    
	        if ($image) {
		        $block
		        	->setContent($form->get('content')->getData())
		        	->setBlockType($blockType)
					->setImage($image_name);
	        }else {
		        $block
		        	->setContent($form->get('content')->getData())
		        	->setBlockType($blockType);
			}


	        $em = $this->getDoctrine()->getManager();
	        $em->persist($block);
	        $em->flush();

	    		return $this->RedirectToRoute('admin-edit-blog', ['id' => $block->getPage()->getId()]);
        }

    	return $this->render('admin/edit-contentblock.html.twig', [
    		'form' => $form->createView(),
			'block' => $block,
			'message' => 'Editando bloque',
			'icon' => '<i class="fas fa-exclamation-circle text-info"></i>'
	      ]);
    }


	    public function createContentBlock(Page $page, Request $request){
	        $blocks_type = $this->getDoctrine()->getRepository(BlockType::class);

	       	$form = $this->createFormBuilder()
				->add('image', FileType::class,[
					'label' => 'Foto:',
					'required' => false ])
				->add('content', TextareaType::class,['label' => 'Contenido:'])
				->add('blockType', ChoiceType::class,[
		              'placeholder' => 'Elige un tipo de bloque',
		              'choices' => $blocks_type->findAll(),
		              'choice_value' => 'id',
		              'choice_label' => 'name'])
	            ->getForm();

	                    // Comprobamos la solicitud
	        $form->handleRequest($request);

	        if ( $form->isSubmitted() && $form->isValid() ) {

	        	$content_block = new ContentBlock();

	        	$image = $form->get('image')->getData();

		        if ($image){
		            // Revisamos la extensión y creamos el nombre del archivo
		            $image_name = '1' . '.jpg';
			        $counter = 1;

			        while (file_exists ('assets/img/blog/content_blocks/' . $page->getId() . '/' . $image_name  ) ) {
			            $counter = $counter + 1;
			            $image_name = $counter . '.jpg';
			        }
		            // Movemos el archivo donde queremos que esté
		            $image->move('assets/img/blog/content_blocks/' . $page->getId() . '/', $image_name);
		            $content_block->setImage($image_name);
		        }else{
		        	$content_block->setImage("");
		        }

		        $blockType = $blocks_type->findOneBy(['id' => $form->get('blockType')->getData()]);

		        $content_block
		        	->setContent($form->get('content')->getData())
		        	->setPage($page)
		        	->setBlockType($blockType);

		        $em = $this->getDoctrine()->getManager();
		        $em->persist($content_block);
		        $em->flush();

	    		return $this->RedirectToRoute('admin-edit-blog', ['id' => $page->getId()]);


	        }

	    	return $this->render('admin/create-contentblock.html.twig', [
	    		'blockTypes' => $blocks_type->findAll(),
	    		'form' => $form->createView()
		      ]);
	    }


	public function getPostByCategory(Category $category, $page){


		$posts_repo = $this->getDoctrine()->getRepository(Page::class);
        $posts = $posts_repo->findBy(
                        ['active' => 1, 'category' => $category],
                        ['id' => 'DESC'],
                        5,
                        5 * ($page - 1)
                      );
        $totalPages = ceil(count($posts_repo->findBy(['active' => 1, 'category' => $category]))/5);

        return $this->render('blog/blog-category-detail.html.twig', [
            'posts' => $posts,
            'totalPages' => $totalPages,
            'currentPage' => $page,
            'category' => $category
        ]);


	}


	public function checkPostSlug($slug){

		$posts_repo = $this->getDoctrine()->getRepository(Page::class);
        $post = $posts_repo->findBy(['slug' => $slug]);

        if (sizeof($post) > 0) {
			return new JsonResponse(['exist' => true]);
        } else {
			return new JsonResponse(['exist' => false]);
        }


	}


}
