<?php

namespace App\Controller;

// Importamos las clases relativas a respuestas y peticiones HTTP
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

// Importamos las entidades necesarias
use App\Entity\Business;
use App\Entity\BusinessCategory;
use App\Entity\Review;
use App\Controller\HelperController;



//Importamos los campos de formularios que vamos a usar
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Security;

class BusinessController extends AbstractController
{
    public function businessDetail(Business $business = NULL,Request $request, Security $security,HelperController $helper){

        if ($security->getUser()) {
            $user = $security->getUser();
        } else {
            $user = NULL;
        }

        $loggedUserReview = $this->getDoctrine()
                        ->getRepository(Review::class)
                        ->findOneBy(['user' => $security->getUser(),'business' => $business]);

        $defContent = "";
        $defChoice = "";
        $editting = false;
        if ($loggedUserReview == NULL || !$loggedUserReview) {
            $loggedUserReview = false;
        } else {
            $defContent = $loggedUserReview->getContent();
            $defChoice = $loggedUserReview->getScore();
            $editting = true;
        }

        if ($business == NULL || !$business){
            return $this->RedirectToRoute('not-found',[
                "message" => "La página del negocio no existe."
            ]);
        } else if ($business->getActive() == false) {
            return $this->RedirectToRoute('not-found',[
                "message" => "La página del negocio no está activa."
            ]);
        }

        $form = $this->createFormBuilder()
            ->add('reviewContent', TextareaType::class,[
                    'data' => $defContent
            ])
            ->add('score', ChoiceType::class, ['choices' => [
                    "★★★★★" => 5,
                    "★★★★✩" => 4,
                    "★★★✩✩" => 3,
                    "★★✩✩✩" => 2,
                    "★✩✩✩✩" => 1
                    ]])
            ->add('image', FileType::class, ['required' => false])
                ->getForm();

        // Comprobamos la solicitud
        $form->handleRequest($request);

        // Comrpobamos si el formuario se ha registrado y es valido
        if ($form->isSubmitted() && $form->isValid()) {


            if ($editting) {
                if ($form->get('image')->getData()) {
                    // Recuperamos el archivo
                    $image = $form->get('image')->getData();

                    // Revisamos la extensión y creamos el nombre del archivo
                    $imageName = $user->getId() . '-' .  $business->getName() . '.' . $image->guessExtension();

                    $counter = 0;

                    while (file_exists ('assets/img/reviews/' . $imageName  ) ) {
                        $counter = $counter + 1;
                        $imageName = $user->getId() . '-' .  $business->getName() . "-" . $counter . ".jpg";
                    }

                    $image->move('assets/img/reviews', $imageName);
                } else {
                    $imageName = NULL;
                }

                $loggedUserReview
                    ->setContent($form->get('reviewContent')->getData())
                    ->setImage($imageName)
                    ->setScore($form->get('score')->getData());

                $em = $this->getDoctrine()->getManager();
                $em->persist($loggedUserReview);
                $em->flush();
            }

            if ($loggedUserReview) {

                $reviews_repo = $this->getDoctrine()->getRepository(Review::class);
                $reviews = $reviews_repo->findBy(['business' => $business, 'active' => 1], ['id' => 'DESC']);

                return $this->render('business/business-detail.html.twig', [
                    'controller_name' => 'BusinessController',
                    'business' => $business,
                    'reviews' => $reviews,
                    'form' => $form->createView(),
                    'loggedUserReview' => $loggedUserReview
                ]);
            }



            if ($form->get('image')->getData()) {
                // Recuperamos el archivo
                $image = $form->get('image')->getData();

                // Revisamos la extensión y creamos el nombre del archivo
                $imageName = $user->getId() . '-' .  $business->getName() . '.' . $image->guessExtension();

                $counter = 0;

                while (file_exists ('assets/img/reviews/' . $imageName  ) ) {
                    $counter = $counter + 1;
                    $imageName = $user->getId() . '-' .  $business->getName() . "-" . $counter . ".jpg";
                }

                $image->move('assets/img/reviews', $imageName);
            } else {
                $imageName = NULL;
            }


            $review = new Review();
            $review
                ->setUser($user)
                ->setBusiness($business)
                ->setContent($form->get('reviewContent')->getData())
                ->setImage($imageName)
                ->setScore($form->get('score')->getData())
                ->setActive(1);

                // Actualizamos el registro en la bbdd con el nombre de la imagen correcto
                $em = $this->getDoctrine()->getManager();
                $em->persist($review);
                $em->flush();
        }


        $reviews_repo = $this->getDoctrine()->getRepository(Review::class);
        $reviews = $reviews_repo->findBy(['business' => $business, 'active' => 1], ['id' => 'DESC']);

        $loggedUserReview = $this->getDoctrine()
                        ->getRepository(Review::class)
                        ->findOneBy(['user' => $security->getUser(),'business' => $business]);

        if (!$loggedUserReview) {
            $loggedUserReview = false;
        }

        return $this->render('business/business-detail.html.twig', [
            'controller_name' => 'BusinessController',
            'business' => $business,
            'reviews' => $reviews,
            'form' => $form->createView(),
            'loggedUserReview' => $loggedUserReview
        ]);
    }

    public function activeBusiness(Business $business){

        if ($business->getActive())
            $business->setActive(0);
        else
            $business->setActive(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($business);
        $em->flush();

        return $this->RedirectToRoute('admin-businesses');
    }

    public function newBusiness(Request $request, HelperController $helper){

        $category_repo = $this->getDoctrine()->getRepository(BusinessCategory::class);
        $categories = $category_repo->findAll();

        $form = $this->createFormBuilder()
        ->add('name', TextType::class)
        ->add('description', TextareaType::class)
        ->add('image', FileType::class)
        ->add('direction', TextType::class)
        ->add('instagram', TextType::class)
        ->add('phone', TextType::class)
        ->add('web', TextType::class)
        ->add('active', CheckboxType::class, ['required' => false])
        ->add('category', ChoiceType::class,[
                'placeholder' => 'Elige una categoría',
                'choices' => $categories,
                'choice_value' => 'id',
                'choice_label' => 'name'])
            ->getForm();

        // Comprobamos la solicitud
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $business_repo = $this->getDoctrine()->getRepository(Business::class);

            if ($business_repo->findBy(['name' => $form->get('name')->getData()])){
                return $this->render('admin/create-business.html.twig', [
                    'form' => $form->createView(),
                    'message' => 'Ya existe un negocio con este nombre.',
                    'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>'
                ]);
            }

            $slug = $form->get('name')->getData();
            $slug = $helper->curateSlug($slug);

            // Recuperamos el archivo
            $image = $form->get('image')->getData();

            // Revisamos la extensión y creamos el nombre del archivo
            $image_name = $slug . '.' . $image->guessExtension();

            // Movemos el archivo donde queremos que esté
            $image->move('assets/img/business', $image_name);

            $business = new Business();
            $business
                ->setName($form->get('name')->getData())
                ->setDescription($form->get('description')->getData())
                ->setDirection($form->get('direction')->getData())
                ->setPhone($form->get('phone')->getData())
                ->setImage($image_name)
                ->setInstagram($form->get('instagram')->getData())
                ->setSlug($slug)
                ->setWeb($form->get('web')->getData())
                ->setActive($form->get('active')->getData())
                ->setCategory($form->get('category')->getData());

                $entityManager= $this->getDoctrine()->getManager();
                $entityManager->persist($business);
                $entityManager->flush();

                // Si se ha creado el usuario se redirige al listado de usuarios
                return $this->RedirectToRoute('admin-businesses',[
                    'targeturl' => 'negocios', // '--' sustituye a '/' para no tener problemas de url
                    'modalTitle' => 'Creado con éxito',
                    'modalMessage' => 'Negocio creado con éxito',
                    'icon' => 'success' //puede ser info,success,danger
                ]);
        } else {
            // Si aún no se ha enviado el formulario devolvemos la vista
            return $this->render('admin/create-business.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

    public function adminEditBusiness(Request $request, Business $business, HelperController $helper){

        $category_repo = $this->getDoctrine()->getRepository(BusinessCategory::class);
        $business_repo = $this->getDoctrine()->getRepository(Business::class);
        $categories = $category_repo->findAll();

        $form = $this->createFormBuilder()
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('image', FileType::class)
            ->add('direction', TextType::class, ['required' => false])
            ->add('phone', TextType::class, ['required' => false])
            ->add('web', TextType::class)
            ->add('instagram', TextType::class)
            ->add('active', CheckboxType::class, ['required' => false])
            ->add('category', EntityType::class,[
                'class' => BusinessCategory::class,
                'choice_label' => 'name',
                'data' => $business->getCategory()
                ])
            ->getForm();

        // Comprobamos la solicitud
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $nameNewAndExists = false;
            if ($business_repo->findBy(['name' => $form->get('name')->getData()])){
                if ($business->getName() !=  $form->get('name')->getData()){
                    $nameNewAndExists = true;
                }
            }

            if ($nameNewAndExists){
                return $this->render('admin/edit-business.html.twig', [
                    'form' => $form->createView(),
                    'message' => 'Otro negocio tiene este nombre.',
                    'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>',
                    'business' => $business
                ]);
            }

            $slug = $form->get('name')->getData();
            $slug = $helper->curateSlug($slug);

            $business
                ->setName($form->get('name')->getData())
                ->setDescription($form->get('description')->getData())
                ->setDirection($form->get('direction')->getData())
                ->setPhone($form->get('phone')->getData())
                ->setSlug($slug)
                ->setInstagram($form->get('instagram')->getData())
                ->setWeb($form->get('web')->getData())
                ->setActive($form->get('active')->getData())
                ->setCategory($form->get('category')->getData());

            // Recuperamos el archivo
            $image = $form->get('image')->getData();

            if ($image){
                // Revisamos la extensión y creamos el nombre del archivo
                $image_name = $slug . '.' . $image->guessExtension();

                // Movemos el archivo donde queremos que esté
                $image->move('assets/img/business', $image_name);
                $business->setImage($image_name);
            }

            $entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($business);
            $entityManager->flush();

            return $this->render('admin/edit-business.html.twig', [
                'form' => $form->createView(),
                'message' => 'Negocio editado con éxito!',
                'message_icon' => '<i class="fas fa-check text-success"></i>',
                'business' => $business
            ]);

        } else {

        // Si aún no se ha enviado el formulario devolvemos la vista
        return $this->render('admin/edit-business.html.twig', [
            'form' => $form->createView(),
            'business' => $business
        ]);

        }
    }

    public function getBusinesses($page){
        $business_repo = $this->getDoctrine()->getRepository(Business::class);
        $businesses = $business_repo->findBy(
                        array(),
                        array('id' => 'ASC'),
                        5,
                        5 * ($page - 1)
                      );
        $totalPages = ceil(count($business_repo->findAll())/5);

        return $this->render('admin/businesses.html.twig', [
            'businesses' => $businesses,
            'totalPages' => $totalPages,
            'currentPage' => $page
        ]);
  }
}
