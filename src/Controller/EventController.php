<?php

namespace App\Controller;

// Importamos las clases relativas a respuestas y peticiones HTTP
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

//Clases importadas por symfony
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

// Importamos las entidades necesarias
use App\Entity\Event;
use App\Entity\User;
use App\Entity\EventCategory;

// Importamos los tipos necesarios para los formularios
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EventController extends AbstractController
{
      public function eventDetail(Event $event = NULL){

        if ($event == NULL || !$event){
          return $this->RedirectToRoute('not-found',[
            "message" => "La página del evento no existe."
          ]);
        } else if ($event->getActive() == false) {
          return $this->RedirectToRoute('not-found',[
            "message" => "La página del evento no está activa."
          ]);
        }

        $event_repo = $this->getDoctrine()->getRepository(Event::class);
        $related_events = $event_repo->findBy(['active' => 1], ['id' => 'DESC']);

        $i = 0;
        $events = array();
        while ( sizeof($events) < 2) {
          if ($related_events[$i]->getId() != $event->getId()) {
            array_push($events, $related_events[$i]) ;

          }
          $i++;
        }


        return $this->render('event/event-detail.html.twig', [
            'event' => $event,
            'related_events' => $events
        ]);
      }

      public function newEvent(Request $request, Security $security, HelperController $helper){

        $user = $security->getUser();
        $event_category_repo = $this->getDoctrine()->getRepository(EventCategory::class);

        $event_categories = $event_category_repo->findAll();

        $form = $this->createFormBuilder()
          ->add('name', TextType::class,['label' => 'Nombre:'])
          ->add('category', EntityType::class,[
                  'class' => EventCategory::class,
                  'choice_label' => 'title'
              ])
          ->add('description', TextareaType::class,['label' => 'Descripción:'])
          ->add('image', FileType::class,['label' => 'Imagen:'])
          ->add('date', DateType::class,['label' => 'Fecha:'])
          ->add('time', TimeType::class,['label' => 'Hora:'])
          ->add('place', TextType::class,['label' => 'Lugar:'])
          ->add('direction', TextType::class,['label' => 'Dirección:'])
          ->getForm();


        // Comprobamos la solicitud
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $slug = $form->get('name')->getData();
            $slug = $helper->curateSlug($slug);

            $event_date = $form->get('date')->getData()->format('Y-m-d');
            $event_time = $form->get('time')->getData()->format('H:i:s');
            $event_dateTime = new \DateTime($event_date . " " . $event_time);

          // Recuperamos el archivo
          $image = $form->get('image')->getData();

          // Revisamos la extensión y creamos el nombre del archivo
          $image_name = $slug . '.' . $image->guessExtension();

          // Movemos el archivo donde queremos que esté
          $image->move('assets/img/event', $image_name);

          $event = new Event();
          $event
            ->setName($form->get('name')->getData())
            ->setSlug($slug)
            ->setImage($image_name)
            ->setCategory($form->get('category')->getData())
            ->setUser($user)
            ->setDescription($form->get('description')->getData())
            ->setEventDate($event_dateTime)
            ->setCreateDate(new \DateTime('NOW'))
            ->setPlace($form->get('place')->getData())
            ->setActive(1)
            ->setDirection($form->get('direction')->getData());

          $entityManager= $this->getDoctrine()->getManager();
          $entityManager->persist($event);
          $entityManager->flush();


          // Si se ha creado el usuario se redirige al listado de usuarios
          return $this->RedirectToRoute('admin-events',[
              'targeturl' => 'eventos', // '--' sustituye a '/' para no tener problemas de url
              'modalTitle' => 'Creado con éxito',
              'modalMessage' => 'Evento creado con éxito',
              'icon' => 'success' //puede ser info,success,danger
          ]);

        } else {

          // Si aún no se ha enviado el formulario devolvemos la vista
          return $this->render('admin/create-event.html.twig', [
            'form' => $form->createView(),
          ]);

        }

      }


  public function activeEvent(Event $event){

    if ($event->getActive())
      $event->setActive(0);
    else
      $event->setActive(1);

    $em = $this->getDoctrine()->getManager();
    $em->persist($event);
    $em->flush();

    return $this->RedirectToRoute('admin-events');

  }

  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////

  public function adminEditEvent(Request $request, Event $event, HelperController $helper){

    $event_category_repo = $this->getDoctrine()->getRepository(EventCategory::class);
    $event_repo = $this->getDoctrine()->getRepository(Event::class);

    $event_categories = $event_category_repo->findAll();

    $form = $this->createFormBuilder()
      ->add('name', TextType::class,['label' => 'Nombre:'])
      ->add('category', EntityType::class,[
              'class' => EventCategory::class,
              'choice_label' => 'title',
              'data' => $event->getCategory()
          ])
      ->add('description', TextareaType::class,['label' => 'Descripción:'])
      ->add('image', FileType::class,['label' => 'Imagen:'])
      ->add('date', DateType::class,['label' => 'Fecha:'])
      ->add('time', TimeType::class,['label' => 'Hora:'])
      ->add('place', TextType::class,['label' => 'Lugar:'])
      ->add('direction', TextType::class,['label' => 'Dirección:'])
      ->getForm();




    // Comprobamos la solicitud
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

        $nameNewAndExists = false;
        if ($event_repo->findBy(['name' => $form->get('name')->getData()])){
            if ($event->getName() !=  $form->get('name')->getData()){
                $nameNewAndExists = true;
            }
        }

        if ($nameNewAndExists){
            return $this->render('admin/edit-event.html.twig', [
                'form' => $form->createView(),
                'message' => 'Otro evento tiene este nombre.',
                'message_icon' => '<i class="fas fa-exclamation-circle text-danger"></i>',
                'user' => $user
            ]);
        }

        $slug = $form->get('name')->getData();
        $slug = $helper->curateSlug($slug);

        $event_date = $form->get('date')->getData()->format('Y-m-d');
        $event_time = $form->get('time')->getData()->format('h:i:s');
        $event_dateTime = new \DateTime($event_date . " " . $event_time);

        $event
        ->setName($form->get('name')->getData())
        ->setSlug($slug)
        ->setCategory($form->get('category')->getData())
        ->setDescription($form->get('description')->getData())
        ->setEventDate($event_dateTime)
        ->setCreateDate(new \DateTime('NOW'))
        ->setPlace($form->get('place')->getData())
        ->setActive(1)
        ->setDirection($form->get('direction')->getData());



        // Recuperamos el archivo
        $image = $form->get('image')->getData();

        if ($image){
            // Revisamos la extensión y creamos el nombre del archivo
            $image_name = $slug . '.' . $image->guessExtension();

            // Movemos el archivo donde queremos que esté
            $image->move('assets/img/event', $image_name);
            $event->setImage($image_name);
        }

        $entityManager= $this->getDoctrine()->getManager();
        $entityManager->persist($event);
        $entityManager->flush();

        return $this->render('admin/edit-event.html.twig', [
            'form' => $form->createView(),
            'message' => 'Evento editado con éxito!',
            'message_icon' => '<i class="fas fa-check text-success"></i>',
            'event' => $event
        ]);

    } else {

      // Si aún no se ha enviado el formulario devolvemos la vista
      return $this->render('admin/edit-event.html.twig', [
        'form' => $form->createView(),
        'event' => $event
      ]);

    }

  }

  public function getEvents($page){
      $event_repo = $this->getDoctrine()->getRepository(Event::class);

      $events = $event_repo->findBy(
                        array(),
                        array('id' => 'ASC'),
                        5,
                        5 * ($page - 1)
                      );
      $totalPages = ceil(count($event_repo->findAll())/5);


      return $this->render('admin/events.html.twig', [
          'events' => $events,
          'totalPages' => $totalPages,
          'currentPage' => $page
      ]);
  }

}
