<?php
/*return $this->render('home/test.html.twig', [
    'var' => $business
]);*/
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\BusinessCategory;
use App\Entity\Business;
use App\Entity\Review;

class BusinessCategoryController extends AbstractController
{
    /**
     * @Route("/business/category", name="business_category")
     */
    public function index()
    {
        return $this->render('business_category/index.html.twig', [
            'controller_name' => 'BusinessCategoryController',
        ]);
    }

    public function category(BusinessCategory $category = NULL,Review $review = NULL)
    {
      if ($category == NULL || !$category){
        return $this->RedirectToRoute('not-found',[
          "message" => "La página de esta categoría no existe."
        ]);
      }

      $business_repo = $this->getDoctrine()->getRepository(Business::class);
			$businesses = $business_repo->findBy(array('category' => $category, 'active' => 1));

      foreach ($businesses as $business) {
        $total_score = 0;
        $review_repo = $this->getDoctrine()->getRepository(Review::class);
        $reviews = $review_repo->findBy(array('business' => $business));

        foreach ($reviews as $review) {
          $total_score = $total_score+$review->getScore();
        }

        if (count($reviews) > 0){
          $avg_score = $total_score / count($reviews); //round();
          $business->setScore($avg_score);
          if ($reviews){
            $business->setNReviews(count($reviews));
          }
        }
      }

        return $this->render('business_category/index.html.twig', [
            'businesses' => $businesses,
            'category' => $category,
        ]);
    }


    public function getCategories(){

      $categories_repo = $this->getDoctrine()->getRepository(BusinessCategory::class);
      $categories = $categories_repo->findAll();



        return $this->render('business/index.html.twig', [
            'categories' => $categories,
        ]);

    }
}
