<?php

namespace App\Controller;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\HelperController;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MailerController extends AbstractController{


	public function welcomeMessage($email, MailerInterface $mailer){
	    $mail = (new TemplatedEmail())
	        ->from('uncolorespecialFCT@gmail.com')
	        ->to($email)
	        //->cc('cc@example.com')
	        //->bcc('bcc@example.com')
	        //->replyTo('fabien@example.com')
	        //->priority(Email::PRIORITY_HIGH)
	        ->subject('Bienvenido a Un Color Especial')
	        ->htmlTemplate('mailer/welcome.html.twig');

	    $mailer->send($mail);

	    return $this->RedirectToRoute('login',[
			'targeturl' => 'iniciar-sesion', // '--' sustituye a '/' para no tener problemas de url
			'modalTitle' => 'Usuario creado',
			'modalMessage' => 'Te has registrado correctamente, checka tu email para activar tu cuenta y luego puedes iniciar sesión.',
			'icon' => 'success' //puede ser info,success,danger
		]);

	}


	public function contactForm(Request $request, MailerInterface $mailer ){

		if ($request->request->has('name')) {

            $data = $request->request;

			$mail = (new TemplatedEmail())
	        ->from($data->get('email'))
	        ->to('uncolorespecialFCT@gmail.com')
	        //->cc('cc@example.com')
	        //->bcc('bcc@example.com')
	        ->replyTo($data->get('email'))
	        //->priority(Email::PRIORITY_HIGH)
	        ->subject('Formulario de contacto' . $data->get('name'))
	        ->htmlTemplate('mailer/contact-form.html.twig')
	        ->context([
	        	'message' => $data->get('message'),
	        	'name' => $data->get('name'),
	        	'useremail' => $data->get('email')
	        ]);
	  		$mailer->send($mail);
        	return $this->render('contact/contact-form.html.twig', [
        		'msg' => 'Se ha enviado el mensaje'
        	]);
        }


        return $this->render('contact/contact-form.html.twig', [
        ]);
    }

    	public function recoverPassword(Request $request, MailerInterface $mailer, HelperController $helper, UserPasswordEncoderInterface $encoder){


    	  	if ($request->request->get('email')) {



	  			$email = $request->request->get('email');
		   		$user_repo = $this->getDoctrine()->getRepository(User::class);
		  		$user = $user_repo->findOneBy(['email' => $email]);

		  		if (!is_null($user)) {

		  			$newPass = $helper->generatePassword();
		  			$encodedPw = $encoder->encodePassword($user, $newPass);
		  			$user->setPassword($encodedPw);
		  			$em = $this->getDoctrine()->getManager();
		  			$em->persist($user);
		  			$em->flush();

	  			    $mail = (new TemplatedEmail())
			        ->from('uncolorespecialFCT@gmail.com')
			        ->to($email)
			        //->cc('cc@example.com')
			        //->bcc('bcc@example.com')
			        //->replyTo('fabien@example.com')
			        //->priority(Email::PRIORITY_HIGH)
			        ->subject('Recuperar contraseña | Un Color Especial')
			        ->htmlTemplate('mailer/recover.html.twig')
			        ->context([
			        	'pass' => $newPass,
			        	'name' => $user->getName(),
			        ]);

				    $mailer->send($mail);


		  			return new Response('{"status" : true}');
		  		} else {
		  			return new Response('{"status" : false}');
		  		}

	  		}else {
		  			return new Response('{"status" : "Bad Request"}');
	  		}





	}

}
