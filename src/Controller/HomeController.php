<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Page;
use App\Entity\Event;
use App\Entity\Business;
use App\Entity\Review;
use App\Entity\BusinessCategory;
use App\Entity\EventCategory;

class HomeController extends AbstractController
{

    public function index(){

        $review_repo = $this->getDoctrine()->getRepository(Review::class);
        $review = $review_repo->findOneBy(['active' => 1],['id' => 'DESC'],1);

        $page_repo = $this->getDoctrine()->getRepository(Page::class);
        $page = $page_repo->findOneBy(['active' => 1],['id' => 'DESC']);

        $event_repo = $this->getDoctrine()->getRepository(Event::class);
        $events = $event_repo->findBy(['active' => 1],['id' => 'DESC'],3);

        $business_repo = $this->getDoctrine()->getRepository(Business::class);
        $business = $business_repo->findBy(['active' => 1]);
        shuffle($business);

        $random_business =  array();
        $total_score = 0;
        for ($i=0; $i < 3 ; $i++) {
          $business_review = $review_repo->findBy(['business' => $business[$i]]);

          foreach ($business_review as $review) {
            $total_score = $total_score+$review->getScore();
          }

          if (count($business_review) > 0){
            $avg_score = $total_score / count($business_review); //round();
            $business[$i]->setScore($avg_score);
            if ($business_review){
              $business[$i]->setNReviews(count($business_review));
            }
          }

          $random_business[$i] = $business[$i];
        }

        return $this->render('home/index.html.twig', [
          'review' => $review,
          'page' => $page,
          'events' => $events,
          'businesses' => $random_business
        ]);
    }

    public function notFound($message = ""){

      return $this->render('home/404.html.twig', [
          'message' => $message,
      ]);
    }

    public function notLogged($message = ""){

      return $this->render('home/401.html.twig', [
          'message' => $message,
      ]);
    }

    public function appIndex(HelperController $helper){

        $BCategories = $this
                        ->getDoctrine()
                        ->getRepository(BusinessCategory::class)
                        ->findAll();

        $ECategories = $this
                        ->getDoctrine()
                        ->getRepository(EventCategory::class)
                        ->findAll();

        $LastBlogs = $this
                        ->getDoctrine()
                        ->getRepository(Page::class)
                        ->findBy(['active' => 1],['id' => 'DESC'],5);

        return $this->render('app/index.html.twig', [
            'Bcats' => $BCategories,
            'Ecats' => $ECategories,
            'lastPosts' => $LastBlogs
        ]);
    }


    public function info(){
        return $this->render('home/info.html.twig', [
        ]);
    }
}
