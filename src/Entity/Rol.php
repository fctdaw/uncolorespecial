<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rol
 *
 * @ORM\Table(name="rol")
 * @ORM\Entity
 */
class Rol
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_rol", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=65, nullable=true)
     */
    private $shown_name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShownName(): ?string
    {
        return $this->shown_name;
    }

    public function setShownName(?string $shown_name): self
    {
        $this->shown_name = $shown_name;

        return $this;
    }


}
