//menu_bar click

$('.menu_bar').click(function(){
	$('nav').toggle();
});


// Scroll
$(document).ready(function(){
	// Comprobamos el ancho de la pantalla para que el efecto solo se ejecute en pantallas grandes
	if ($(window).width() > 800) {
		// Esto es lo mismo que en css poner ** height: 100vh; **
		//$("#banner").css({"height":$(window).height() + "px"});

		var flag = false;
		var scroll;

		$(window).scroll(function(){
			scroll = $(window).scrollTop();

			if(scroll > 40){
				if(!flag){
					$("#logo").css({"margin-top": "-5px", "width": "50px","height":"50px"});
					$("#main-menu").css({"background-color": "#f28B0C"});
					$('header').addClass('reduce-menu');
					$('main').css({'padding-top':'20vh'});
					flag = true;
				}
			}else{
				if(flag){
					$("#logo").css({"margin-top": "10vh", "width": "150px","height":"150px"});
					$("#main-menu").css({"background-color": "rgba(242, 139, 12, .7)"});
					$('header').removeClass('reduce-menu');
					$('main').css({'padding-top':'36vh'});

					flag = false;
				}
			}


		});
	}

	//Cambia el label para que se muestre el nombre de la foto subida

	$('#fotoReview input').change(function(){
		var filename = $(this).val().split('\\');
		arr = filename.length - 1;
		$('#fotoReview label').html(filename[arr]);
	});


	//Hace que se muestre la preview de la foto subida

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#fotoPreview').removeClass('d-none');
				$('#fotoPreview').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#fotoReview input").change(function() {
		readURL(this);
	});


	//


	$('.menu-button').click(function(){
		$('header').toggleClass('menu-fixed');
	});

});

// Formulario inicio sesión check

function showPassword() {

    var key_attr = $('#password').attr('type');

    if(key_attr != 'text') {

        $('.checkbox').addClass('show');
        $('#password').attr('type', 'text');

    } else {

        $('.checkbox').removeClass('show');
        $('#password').attr('type', 'password');

    }

}

// Efecto de abrir la imagen zoom*

	$('.myImg').click(function(){
		$("#myModal").show();
		$('#img01').attr('src', $(this).attr('src'));
		$('#caption').text($(this).attr('alt'));

	});

	$('.myModal').click(function(){
		$(this).hide();
		$(".popup-content").hide();
	});

/* Inicializa el script para animaciones al hacer scroll.
Solo hay que agregar a la clase CSS .wow a un elemento HTML:
será invisible hasta que el usuario se desplace para revelarla.
Ejemplo: <img src="..." class="wow"> */
new WOW().init();


function contarCaracteres(textarea, imprimir) {
		$(textarea).keyup(function(){
		var longitud = $(textarea).val().length;
		$(imprimir).html(longitud);
	 });
}


//Ordena los negocios de menor a mayor y viceversa
$("button.menor").click(function(){
	var $wrapper = $('.scroll-box');

	$wrapper.find('.categoryBusinessCard').sort(function (a, b) {
		return +a.dataset.name - +b.dataset.name;
	})
	.appendTo( $wrapper );
});

$("button.mayor").click(function(){
	var $wrapper = $('.scroll-box');

	$wrapper.find('.categoryBusinessCard').sort(function (a, b) {
		return +b.dataset.name - +a.dataset.name;
	})
	.appendTo( $wrapper );
});


//Filtros para ocultar o mostrar negocios

$("button.filterSelectionTodos").click(function(){
	$('.puntuacion-0').show();
	$('.puntuacion-1').show();
	$('.puntuacion-2').show();
	$('.puntuacion-3').show();
	$('.puntuacion-4').show();
	$('.puntuacion-5').show();
	setTimeout(elementsHidden,250);

});

$("button.filterSelection1").click(function(){
	$('.puntuacion-1').show();
	$('.puntuacion-0').hide();
	$('.puntuacion-2').show();
	$('.puntuacion-3').show();
	$('.puntuacion-4').show();
	$('.puntuacion-5').show();
	setTimeout(elementsHidden,250);
});

$("button.filterSelection2").click(function(){
	$('.puntuacion-2').show();
	$('.puntuacion-0').hide();
	$('.puntuacion-1').hide();
	$('.puntuacion-3').show();
	$('.puntuacion-4').show();
	$('.puntuacion-5').show();
	setTimeout(elementsHidden,250);
});

$("button.filterSelection3").click(function(){
	$('.puntuacion-3').show();
	$('.puntuacion-0').hide();
	$('.puntuacion-1').hide();
	$('.puntuacion-2').hide();
	$('.puntuacion-4').show();
	$('.puntuacion-5').show();
	setTimeout(elementsHidden,250);
});

$("button.filterSelection4").click(function(){
	$('.puntuacion-4').show();
	$('.puntuacion-0').hide();
	$('.puntuacion-1').hide();
	$('.puntuacion-2').hide();
	$('.puntuacion-3').hide();
	$('.puntuacion-5').show();
	setTimeout(elementsHidden,250);
});

$("button.filterSelection5").click(function(){
	$('.puntuacion-5').show();
	$('.puntuacion-0').hide();
	$('.puntuacion-1').hide();
	$('.puntuacion-2').hide();
	$('.puntuacion-3').hide();
	$('.puntuacion-4').hide();
	setTimeout(elementsHidden,250);
});

$(document).ready(function(){
	$('#reveditshow').hide(0);
})

$('#editaropinion').click( function(){
	$('#revedithide').slideUp( 'slow' , function(){
		$('#reveditshow').slideDown( 'slow' )
	})
});


$( document ).ready(function(){
	maxHeight=0;
	$(".same-height-cards").each(function(){
		if (maxHeight < $(this).outerHeight()) {
			maxHeight = $(this).outerHeight();
		}
	})

	$(".same-height-cards").css({height: maxHeight});
});
