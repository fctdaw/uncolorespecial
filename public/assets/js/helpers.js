//Contador de Caracteres
function contarCaracteres(textarea, imprimir) {
		$(textarea).keyup(function(){
		var longitud = $(textarea).val().length;
		$(imprimir).html(longitud);
	 });
}


//Comprobacion de Claves Iguales en Formularios
function clavesIguales(clave1, clave2) {
	comparaClaves(clave1, clave2);
	comparaClaves(clave2, clave1);
}

function comparaClaves(clave1, clave2){
		$(clave1).keyup(function(){
		if (clave1.val() == clave2.val()) {
			$("#reg_buttom").removeClass("disabled");
			$("#isdifferent").html("");
		}else{
			$("#reg_buttom").addClass("disabled");
			$("#isdifferent").html("Las contraseñas no coinciden");
		}
	});
}

function clavesIgualesEdit(clave1, clave2){
	comparaClavesEdit(clave1, clave2);
	comparaClavesEdit(clave2, clave1);
}

function comparaClavesEdit(clave1, clave2){
		$(clave1).keyup(function(){
		if (clave1.val() == clave2.val()) {
			$("#isdifferent").html("");
		}else{
			$("#isdifferent").html("Las contraseñas no coinciden");
		}
	});
}


//Funcion para red social Whatsapp
function whatsApp() {
		var urlActual = window.location.href;
		window.open("https://wa.me/?text=" + urlActual, "_blank", "resizable=yes,top=500,left=500,width=400,height=400");
}

//Copiar enlaces al portapapeles
function clipboard() {
	var aux = document.createElement("input");
	aux.setAttribute("value",window.location.href);
	document.body.appendChild(aux);
	aux.select();
	document.execCommand("copy");
	document.body.removeChild(aux);
	$("#clipboard").html("Copiando al portapapeles");
	setTimeout(function() {$("#clipboard").html("<i class='far fa-smile'></i> Copiado!");}, 800);
	setTimeout(function() {$("#clipboard").html("");}, 1500);
}


// EL estiloTiempo
$(document).ready(function(){
	//Meto en una variable la URL con mi código
	urlopenweather = "http://api.openweathermap.org/data/2.5/weather?id=6361046&lang=sp&units=metric&appid=b570cdc03d0d01c17690bad21c57e02c";
	//Solicitud asincrónica de HTTP (Ajax).
	$.ajax({
		//URL a la que se envía la solicitud.
		url: urlopenweather,
		//Evalúa la respuesta como JSON
		datatype: 'json',
		//Si la petición es exitosa, realizamos la función programada:
		success: function(result) {

			////////////////////////////////////////////////
			//// AQUÍ HAY UN ERROR SOLVENTADO CON UN IF ////
			////////////////////////////////////////////////

			if (typeof temperatura !== 'undefined'){
				// Escribo el contenido textual accediendo al atributo del objeto result
				console.log(result);
				//ciudad.textContent = result.name ;

				temperatura.textContent = result.main.temp + " °C";
				humedad.textContent = result.main.humidity + " %";
				viento.textContent = result.wind.speed + " m/s";
				tempMax.textContent = result.main.temp_max + " ° max";
				tempMin.textContent = result.main.temp_min + " ° min";

				urlIcon = "http://openweathermap.org/img/wn/" + result.weather[0].icon + ".png";
				//console.log(urlIcon);
				$("#iconoImg").attr("src",urlIcon);
				iconoLabel.textContent = result.weather[0].description;
			}

			////////////////////////////////////////////////
			//// AQUÍ HAY UN ERROR SOLVENTADO CON UN IF ////
			////////////////////////////////////////////////

		}
	});
});

//Moestra u oculta un DIV si no hay elementos filtrados
function elementsHidden() {

	var total = 0;
	$('.business-category-business-card').each(function(){

		if (!$(this).hasClass('d-none')) {
			total++;
		}

		if (total == 0) {
			$('#empty').parent.removeClass('d-none');
		}else {
			$('#empty').parent.addClass('d-none');
		}
	});

}


//Popup para dispositivos moviles
$(document).ready(function(){
	//Recoge multiples datos del dispositivo en el que entra el usuario
	var ua = new UAParser();
	var result = ua.getResult();
	/* En caso de ser exclusivamente de Android el div popopmobile
	se eliminaria su clase d-none haciendo asi que se muestre*/
	if(result.os.name == "Android"){
		//Almacena en localstorage un valor 'NotShow'
		const user = localStorage.getItem('notShow');
		//Comprueba si esta vacio
		if (user === null) {

			$("#popupmobile").removeClass("d-none");

				$('#notShow').click(function(){
				localStorage.setItem('notShow', "d-none");

				console.log(user);
				$("#popupmobile").addClass("d-none");

			});

		}

	}
});



//Fuerza la introduccion de datos a minuscula (para emails "login")
$(document).ready(function(){
	$('.minus').keyup(function(){
		var lowercase = $('.minus').val().toLowerCase();
		$('.minus').text(lowercase);
		console.log(lowercase)
	 });
 });



//Subir archivo imagen de reviews con jpeg/jpg
function validateFileType(){
    var fileName = document.getElementById("form_image").value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg"){
   		$('#image_error').addClass('d-none');
    }else{
    	$('#form_image').val('');
        $('#image_error').html('Solo se permite jpg/jpeg').removeClass('d-none');
    }
}


//Contraseña valida y mensaje error
$(document).ready(function(){
	$("#form_password, #form_newPassword, #form_confirmPass").keyup(function(){

		if($("#form_password, #form_newPassword").val().length == 0){ //Comprueba que este vacio el input
			$("#invalidpass").html("");
		}else if($("#form_password, #form_newPassword").is(":valid")) { //Comprueba si es valido el pattern
			$("#invalidpass").html("¡Correcto!").addClass("text-success").removeClass("text-danger");
		}else if($("#form_password, #form_newPassword").is(":invalid")){ //Comprueba si es invalido el pattern
			$("#invalidpass").html('La contraseña debe contener:<br>-8 o más carácteres<br>-Mínimo una letra mayúscula<br>-Mínimo un número').addClass("text-danger").removeClass("text-success");
		}
	});
});
