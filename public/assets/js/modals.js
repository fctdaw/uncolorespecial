$( document ).ready(function() {
    $('#infoModal').modal('toggle');
    $('#infoModal').click(function(){
		target = $('#infoModal').data('targeturl');
        target = target.replace(/\-\-/g,'/');
        window.location.href = target;
    });

	$('#infoModal').focusout(function(){
        target = $('#infoModal').data('targeturl');
        target = String(target).replace(/\-\-/g,'/');
        window.location.href = target;
    });
});
